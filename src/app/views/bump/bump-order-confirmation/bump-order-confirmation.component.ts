import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { Router } from '@angular/router';
import { ValidationService } from 'src/app/services/validation.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-bump-order-confirmation',
  templateUrl: './bump-order-confirmation.component.html',
  styleUrls: ['./bump-order-confirmation.component.css']
})
export class BumpOrderConfirmationComponent implements OnInit {

  order_id: number;
  order_status: number;
  update_status: number;
  orderList=[];
  OrderViewList;
  redirectoPage;
  langindex:any=0;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    private toastr: ToastrService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.langindex=sessionStorage.getItem('langIndex')==null?0:sessionStorage.getItem('langIndex');
    this.route.queryParams.subscribe(params => {
      this.order_id = params['order_id'];
      this.order_status= params['order_status'];
      this.redirectoPage= params['redirectoPage'];
      if (this.order_id !== undefined) {
        // this.router.navigate(['/pending-orders']);
      }
      else{
      this.router.navigate(['/order']);
      }
    });
    this.doFetchOrderListByOrderid();
    this.doFetchOrderViewDetails();
    
  }

  doFetchOrderViewDetails() {
   
    this.apiService.doOrderViewList(this.order_id)
      .subscribe(
        (data: any) => {
          this.OrderViewList =data.data.orders;
          // console.log('detilas view');
          // console.log(this.OrderViewList);
        },

        error => {
          this.toastr.error(error.error.error.msg);
          console.log(error);
        }
      );
       
      } 
  doFetchOrderListByOrderid() {
    this.apiService.doGetOrderListByOrderid(this.order_id)
      .subscribe(
        (data: any) => {
          this.orderList =data.data.orders;
          // this.toastr.success('Order Fetched');
          //console.log(this.orderList)
        },
        error => {
          this.toastr.error(error.error.error.msg);
          console.log(error);
        }
      );
      }    
        

      doBump(){
        this.router.navigate(['/bump-kitchen'],{ queryParams: { order_id: this.order_id,order_status:this.order_status },});
      } 
  doPrint(){
    window.print();
  }

}
