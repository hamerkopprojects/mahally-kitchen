import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { Router } from '@angular/router';
import { ValidationService } from 'src/app/services/validation.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { Ng2CompleterModule } from 'ng2-completer';
@Component({
  selector: 'app-order-delivered',
  templateUrl: './order-delivered.component.html',
  styleUrls: ['./order-delivered.component.css']
})
export class OrderDeliveredComponent implements OnInit {

  order_status:number=5;
  messageType = null;
  orderList = [];
  AutoFillData=[];
  CurrentFilterValue:any;
  searchForm: FormGroup;
  currentPage:any;
  totalItems:any;
  OrderitemsPerPage:number=10;
  langindex:any=0;
  isFIlterOn:any=0;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private SpinnerService: NgxSpinnerService
  ) { }

  ngOnInit(): void {
    this.langindex=sessionStorage.getItem('langIndex')==null?0:sessionStorage.getItem('langIndex');
    this.searchForm = this.formBuilder.group({
      order_id: ['', [Validators.required]]
    });

    this.doFetchOrderList();
    this.currentPage=1;
 
  }
  GetOderDeliveredDetails() {  
    this.SpinnerService.show();    
    this.apiService.doGetDeliveredOrderList()
      .subscribe(
        (data: any) => {
          this.orderList =data.data.orders;
          this.SpinnerService.hide();
        }
      );
  }
  doFetchOrderList() {
    this.SpinnerService.show(); 
    this.apiService.doGetDeliveredOrderList()
      .subscribe(
        (data: any) => {
          this.orderList =data.data.orders;
           this.AutoFillData=data.data.orders.map(x=>x.order_id);
           this.totalItems=this.AutoFillData.length;
           this.SpinnerService.hide();
        },
        error => {
          this.toastr.error(error.error.error.msg);
          this.SpinnerService.hide();
          console.log(error);
        }
      );
      }

      doResetsearch(){
        // this.doReload=true;
        this.isFIlterOn=0;
        location.reload();
    
      }
  
  doFetchOrderListByOrderid() {
         this.isFIlterOn=1;
         this.SpinnerService.show(); 
    
        let data: any = this.searchForm.value;
        // Linto this.apiService.doGetOrderListByOrderid(data.order_id)
        this.apiService.doGetOrderListByOrderid(this.CurrentFilterValue)
          .subscribe(
            (data: any) => {
              this.orderList =data.data.orders;
              this.totalItems=data.data.orders.length;
              this.SpinnerService.hide(); 
            },
            error => {
              this.toastr.error(error.error.error.msg);
              console.log(error);
            }
          );
          }    
          onPageChange(number: number) {
            // this.logEvent(`pageChange(${number})`);
            // this.config.currentPage = number;
            console.log('page index change');
            this.currentPage=number;
        }
          
  doBump(order_id : number){
        this.router.navigate(['/bump'],{ queryParams: { order_id: order_id,order_status:this.order_status }});
      }   
      
  doViewDetails(order_id : number){
      this.router.navigate(['/bump-order-confirmation'],{ queryParams:  { order_id: order_id,order_status:this.order_status,redirectoPage:'order_delivered' }});
     } 

}
