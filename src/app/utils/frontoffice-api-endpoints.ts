export class FrontOfficeApiEndPoints {
    public static get login(): string { return "front-desk-staff-login"; }
    public static get forgotpassword(): string { return "forgot-password"; }
    public static get verify(): string { return "front-desk-verify"; }
    public static get resetpassword(): string { return "front-desk-reset-password"; }
    public static get logout(): string { return "frontdesk-logout"; }   
  }