import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { Router } from '@angular/router';
import { ValidationService } from 'src/app/services/validation.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-bump-kitchen',
  templateUrl: './bump-kitchen.component.html',
  styleUrls: ['./bump-kitchen.component.css']
})
export class BumpKitchenComponent implements OnInit {
order_id:any
order_status:any
id:any
langindex:any=0;
kitchenTime:any
update_status: any
UpdatedCookingTime:any
  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    private toastr: ToastrService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.langindex=sessionStorage.getItem('langIndex')==null?0:sessionStorage.getItem('langIndex');
    this.route.queryParams.subscribe(params => {
      this.order_id = params['order_id'];
      this.order_status =params['order_status'];
      if (this.order_id !== undefined) {

      }
      else{
      this.router.navigate(['/order']);
      }
    });
    this.doFetchOrderListByOrderid();
  }

  doFetchOrderListByOrderid() {
    this.apiService.doGetOrderListByOrderid(this.order_id)
      .subscribe(
        (data: any) => {
          // console.log('data fetch in array');
          // console.log(data.data)
          this.id =data.data.orders[0].id;
          this.UpdatedCookingTime=data.data.orders[0].cooking_time;
         },
        error => {
          this.toastr.error(error.error.error.msg);
          console.log(error.error.error.msg);
        }
      );
      }  
  // Time click call only
  doUpdateOrderStatusInKitchen() {
    this.apiService.doUpdateKitchenTime(this.id,this.kitchenTime)
      .subscribe(
        (data: any) => {       
          //console.log(this.kitchenTime); 
        //  this.router.navigate(['/bump-kitchen'],{ queryParams: { order_id: this.order_id,order_status:this.order_status }});
        this.UpdatedCookingTime= this.kitchenTime;
        this.toastr.success(this.langindex==0?'Status changed successfully':'تم تغيير الحالة بنجاح');
        },
        error => {
          this.toastr.error(error);
          console.log(error);
        }
      );
  }

  doSelectKitchenTime(kitchenTime: any){
    this.kitchenTime=kitchenTime;
    console.log( this.kitchenTime);
    this.doUpdateOrderStatusInKitchen();
  }

  // For Updating Button Click Status
  doUpdateOrderStatusInKitchenStatus() {
    this.update_status=3;
    //console.log(this.id,this.update_status);
    this.apiService.doUpdateOrderStatus(this.id,this.update_status)
      .subscribe(
        (data: any) => {
          this.router.navigate(['/order']);
         //  this.router.navigate(['/bump-kitchen'],{ queryParams: { order_id: this.order_id,order_status:this.order_status }});
         

        },
        error => {
          //this.toastr.error(error);
          console.log(error);
        }
      );
  }




//StausConfirm function Ready for pickup
doUpdateOrderStatusReadyforpickup() {
    this.update_status=4;
    this.apiService.doUpdateOrderStatus(this.id,this.update_status)
      .subscribe(
        (data: any) => {
          this.router.navigate(['/order']);
          //this.router.navigate(['/bump-kitchen'],{ queryParams: { order_id: this.order_id }});
          this.toastr.success(this.langindex==0?'Status changed successfully':'تم تغيير الحالة بنجاح');
         // console.log(data)

        },
        error => {
          this.toastr.error(error);
          console.log(error);
        }
      );
  }  
      
  doViewDetails(){
    this.router.navigate(['/bump-order-confirmation'],{ queryParams:  { order_id: this.order_id,order_status:this.order_status }});
   }  
}
