export class KitchenApiEndPoints {
    public static get login(): string { return "kitchen-staff-login"; }
    public static get forgotpassword(): string { return "forgot-password"; }
    public static get verify(): string { return "verify"; }
    public static get resetpassword(): string { return "reset-password"; }
    public static get logout(): string { return "kitchen-logout"; }   
    public static get activeorders(): string { return "active-order-list"; }   
    public static get readyforpickup(): string { return "ready-for-pickup-order-list"; }   
    public static get deliveredorders(): string { return "delivered-order-list"; } 
    public static get searchorders(): string { return "kitchen/search-order-list"; } 
    public static get changestatus(): string { return "kitchen/change-status"; } 
    public static get changetime(): string { return "update_cooking_time"; } 
    public static get OrderViewList(): string { return "kitchen/order-view"; }
    
  }