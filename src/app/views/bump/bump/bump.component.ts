import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { Router } from '@angular/router';
import { ValidationService } from 'src/app/services/validation.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-bump',
  templateUrl: './bump.component.html',
  styleUrls: ['./bump.component.css']
})
export class BumpComponent implements OnInit {

  order_id: number;
  order_status: number;
  update_status: number;
  id: number;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    private toastr: ToastrService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.order_id = params['order_id'];
      this.order_status = params['order_status']
      if (this.order_id !== undefined) {
        // this.router.navigate(['/pending-orders']);
      }
      else{
      this.router.navigate(['/order']);
      }
    });
    this.doFetchOrderListByOrderid();
  }


  //StausConfirm function

  doUpdateOrderStatusInKitchen() {
    this.update_status=3;
    this.apiService.doUpdateOrderStatus(this.id,this.update_status)
      .subscribe(
        (data: any) => {
          this.router.navigate(['/bump-kitchen'],{ queryParams: { order_id: this.order_id,order_status:this.order_status }});
          // this.toastr.success("Status changed successfully");
          console.log(data)

        },
        error => {
          this.toastr.error(error);
          console.log(error);
        }
      );
  }




      doFetchOrderListByOrderid() {
        this.apiService.doGetOrderListByOrderid(this.order_id)
          .subscribe(
            (data: any) => {
              this.id =data.data.orders[0].id;
              this.order_status =data.data.orders[0].order_status;
              // console.log(data.data.orders[0].id)
            },
            error => {
              this.toastr.error(error.error.error.msg);
              console.log(error.error.error.msg);
            }
          );
          }  

          doViewDetails(){
            this.router.navigate(['/bump-order-confirmation'],{ queryParams:  { order_id: this.order_id,order_status:this.order_status }});
           } 
}
