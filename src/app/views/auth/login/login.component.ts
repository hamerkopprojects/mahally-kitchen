import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { Router } from '@angular/router';
import { ValidationService } from 'src/app/services/validation.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from "@angular/router";
import {DOCUMENT} from '@angular/common';
import { BehaviorSubject, Observable } from 'rxjs';
import { IfStmt } from '@angular/compiler';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  message = null;
  messageType = null;
  loginForm: FormGroup;
  activate: string;
  ln:any;
  langindex:any=0;
  isEnglish:boolean;
  private language: BehaviorSubject<string>;
  private routerInfo: BehaviorSubject<string>;

  constructor(
    @Inject(DOCUMENT) private document,
    private router: Router,
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    private toastr: ToastrService,
    private route: ActivatedRoute
  ) {
    this.route.queryParams.subscribe(params => {
      this.activate = params['status'];
      if (this.activate !== undefined && this.activate == 'verified') {
        
        this.toastr.success(this.langindex==0?'Your account has been activated successfully. You can now login':'تم تنشيط حسابك بنجاح. تستطيع الآن تسجيل الدخول');
      }
    });
    this.language = new BehaviorSubject<string>(sessionStorage.getItem('ln'));
    this.routerInfo = new BehaviorSubject<string>(sessionStorage.getItem('ln'));
    this.isEnglish=true;
  }

  ngOnInit(): void {
    this.langindex=sessionStorage.getItem('langIndex')==null?0:sessionStorage.getItem('langIndex');
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, ValidationService.emailValidator]],
      password: ['', [Validators.required]]
    });

    this.ln=sessionStorage.getItem('ln');
    //console.log(this.ln);
    if (this.ln === null){
      this.isEnglish=true;
      this.document.getElementById('theme_bs').setAttribute('href', './assets/stylesheets/bootstrap.min.css');
      this.document.getElementById('theme_main').setAttribute('href', './assets/stylesheets/main.css');
    }
    else if(this.ln === 'ar'){
      this.isEnglish=true;
      this.document.getElementById('theme_bs').setAttribute('href', './assets/stylesheets/bootstrap-rtl.min.css');
      this.document.getElementById('theme_main').setAttribute('href', './assets/stylesheets/main-rtl.css');
    }
    else if(this.ln === 'en'){
      this.isEnglish=false;
      this.document.getElementById('theme_bs').setAttribute('href', './assets/stylesheets/bootstrap.min.css');
      this.document.getElementById('theme_main').setAttribute('href', './assets/stylesheets/main.css');
    }
  }


  setValue(ln: any): void {
    this.routerInfo.next(ln);
  }

  public get getLanguage(): string {
    return this.language.value;
  }

  doLangaugeChange(ln: any){
    if (ln === 'ar'){
      this.isEnglish=false;
      sessionStorage.setItem('ln','ar');
      sessionStorage.setItem('langIndex','1');
      this.document.getElementById('theme_bs').setAttribute('href', './assets/stylesheets/bootstrap-rtl.min.css');
      this.document.getElementById('theme_main').setAttribute('href', './assets/stylesheets/main-rtl.css'); 
    }
    else if (ln === 'en'){
      this.isEnglish=true;
      sessionStorage.setItem('ln','en');
      sessionStorage.setItem('langIndex','0');
      this.document.getElementById('theme_bs').setAttribute('href', './assets/stylesheets/bootstrap.min.css');
      this.document.getElementById('theme_main').setAttribute('href', './assets/stylesheets/main.css');
    }
    this.ln=this.getLanguage;
    this.langindex=sessionStorage.getItem('langIndex')==null?0:sessionStorage.getItem('langIndex');
  }

  //Login function

  doLogin() {
    let data: any = this.loginForm.value;
    this.apiService.doLogin(data.email, data.password)
      .subscribe(
        (data: any) => {
          if(data.success==true)
          {

            this.toastr.success(this.langindex==0?'Login Successfull':'تم تسجيل الدخول بنجاح');
            this.router.navigate(['/order'], { queryParams: { login: true } });
          }
          else{
            this.toastr.error(data.message);
          }
        },
        error => {
          this.toastr.error(error.error.error.msg);
          //console.log(error);
        }
      );
  }

}
