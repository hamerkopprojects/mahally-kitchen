import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiService } from './../services/api.service';

@Injectable()

export class JwtInterceptor implements HttpInterceptor {
    constructor(private authenticationService: ApiService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        let token = this.authenticationService.getTokenValue;
        if (token) {
            request = request.clone({
                setHeaders: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer  ${token}`
                }
            });
        }

        return next.handle(request);
    }
}